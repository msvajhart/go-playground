package main

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"strings"
	"time"
)

//create a new type of 'deck
//which is a slice of strings

type deck []string

func newDeck() deck {
	cardSuits := []string{"Spades", "Clubs", "Hearts", "Diamonds"}
	cardValues := []string{"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"}
	d := deck{}
	for _, suit := range cardSuits {
		for _, value := range cardValues {
			d = append(d, value+" of "+suit)
		}
	}

	return d
}

func (d deck) print() {
	for _, card := range d {
		fmt.Println(card)
	}
}

func deal(d deck, handSize int) (deck, deck) {
	//return two decks, one that goes to the handsize, and then another with the remaining original deck
	return d[:handSize], d[handSize:]
}

func (d deck) shuffle() {
	//generate a new random number generator based in time (all due to how go does rand + seed value)
	source := rand.NewSource(time.Now().UnixNano())
	r := rand.New(source)
	//loop through deck
	for i := range d {
		//get random position for card i
		newPosition := r.Intn(len(d) - 1)
		//swap cards at i position and new random position
		d[i], d[newPosition] = d[newPosition], d[i]
	}
}

func (d deck) saveDeckToFile(filename string) error {
	return ioutil.WriteFile(filename, []byte(d.toString()), 0666)
}

func (d deck) toString() string {
	return strings.Join([]string(d), ",")
}

func newDeckFromFile(filename string) deck {
	content, err := ioutil.ReadFile(filename)
	if err != nil {

		fmt.Println("Error: ", err)
		//Option 1: give them a new deck if it fails
		//return newDeck()
		//Option 2: exit the program
		os.Exit(1)
	}
	return toDeck(string(content))
}

func toDeck(s string) deck {
	return deck(strings.Split(s, ","))
}
