package main

import (
	"fmt"
	"strconv"
)

func main() {
	length := 100
	set := []int{}
	for i := 0; i <= length; i++ {
		set = append(set, i)
	}

	for _, value := range set {
		if value%2 == 0 {
			fmt.Println(strconv.Itoa(value) + " is even")
		} else {
			fmt.Println(strconv.Itoa(value) + " is odd")
		}

	}
}
